from django.db import models
from places.models import Place
from users.models import SimpleUser


# Create your models here.
class Comment(models.Model):
    text = models.TextField(blank=False, null=False)
    place = models.ForeignKey(Place, on_delete=models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(SimpleUser, on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return self.text