from django.test import TestCase
from .models import Comment


class TestComment(TestCase):

    def setUp(self):
        self.comment = Comment.objects.create(text='test comment')

    def tearDown(self):
        self.comment.delete()

    def test_create(self):
        self.assertEqual(self.comment.text, 'test comment')

    def test_str(self):
        self.assertEqual(str(self.comment), 'test comment')
