from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import DeleteView, UpdateView, FormView
from .forms import CommentForm
from .models import Comment
from places.models import Place


class CommentFormView(FormView):
    form_class = CommentForm
    template_name = 'comments/edit.html'
    success_url = '/'

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        text = cleaned_data['text']
        rate = cleaned_data['rate']
        place = Place.objects.filter(id=self.request.path.split('/')[2]).first()
        place.rate = place.rate + rate / 5
        place.save()
        comment = Comment.objects.create(text=text, user=self.request.user, place=place)
        comment.save()
        return super().form_valid(form)


class CommentUpdateView(UpdateView):
    model = Comment
    template_name = 'comments/edit.html'
    success_url = '/'
    fields = ('text',)


class CommentDeleteView(UserPassesTestMixin, DeleteView):
    def test_func(self):
        user = self.request.user
        comment = Comment.objects.filter(id=self.request.path.split('/')[3]).first()
        return (user.email == comment.user.email) or user.is_superuser
    model = Comment
    template_name = 'comments/delete_confirm.html'
    success_url = '/'
