"""interesting_places URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from places.views import PlacesListView, PlaceDetailView,PlaceCreateView, PlaceDeleteView, PlaceUpdateView
from comments.views import CommentFormView, CommentUpdateView, CommentDeleteView
from users.views import UserCreateView, AuthView, SimpleUserLogoutView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', PlacesListView.as_view()),
    path('place/<int:pk>/', PlaceDetailView.as_view()),
    path('place/delete/<int:pk>/', PlaceDeleteView.as_view()),
    path('place/update/<int:pk>/', PlaceUpdateView.as_view()),
    path('place/create/', PlaceCreateView.as_view()),
    path('register/', UserCreateView.as_view()),
    path('login/', AuthView.as_view()),
    path('logout/', SimpleUserLogoutView.as_view()),
    path('comment/update/<int:pk>/', CommentUpdateView.as_view()),
    path('comment/<int:pk>/create/', CommentFormView.as_view()),
    path('comment/delete/<int:pk>/', CommentDeleteView.as_view()),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
