from django.core.management.base import BaseCommand
from places.models import Country, City, Place


class Command(BaseCommand):

    def handle(self, *args, **options):
        russia = Country.objects.create(name='Russia', description='Great country')
        kazan = City.objects.create(name='Kazan', description='3-th center of Russia', country=russia)
        moscow = City.objects.create(name='Moscow', description='Center of Russia', country=russia)
        sochi = City.objects.create(name='Sochi', description='South of Russia', country=russia)