from django.db import models
from users.models import SimpleUser



class Country(models.Model):
    name = models.CharField(max_length=64, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=32, unique=True)
    description = models.TextField(blank=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=64, unique=True)
    description = models.TextField(blank=True)
    rate = models.FloatField(default=0.0)
    img = models.ImageField(upload_to='places', blank=True, null=True)
    cities = models.ManyToManyField(City)

    def cities_count(self):
        return self.cities.all().count()

    def comments(self):
        from comments.models import Comment
        comments = Comment.objects.filter(place=self)
        return comments


    def __str__(self):
        return self.name



