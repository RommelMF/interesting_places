from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.shortcuts import render
from django.http import Http404
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from .models import Place


# Create your views here.
@login_required
def index_view(request):
    if request.method == 'GET':
        places = Place.objects.all()
        return render(request, 'places/index.html', {'places': places})
    else:
        raise Http404


class PlacesListView(ListView):
    model = Place
    template_name = 'places/index.html'


class PlaceDetailView(LoginRequiredMixin, DetailView):
    model = Place
    template_name = 'places/detail.html'


class PlaceCreateView(UserPassesTestMixin, CreateView):
    def test_func(self):
        user = self.request.user
        return user.is_superuser
    model = Place
    template_name = 'places/edit.html'
    success_url = '/'
    fields = ('name', 'description', 'img', 'cities')


class PlaceUpdateView(LoginRequiredMixin, UpdateView):
    model = Place
    template_name = 'places/edit.html'
    success_url = '/'
    fields = '__all__'


class PlaceDeleteView(UserPassesTestMixin, DeleteView):
    def test_func(self):
        user = self.request.user
        return user.is_superuser
    model = Place
    template_name = 'places/delete_confirm.html'
    success_url = '/'
