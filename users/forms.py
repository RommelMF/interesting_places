from django.contrib.auth.forms import UserCreationForm
from .models import SimpleUser


class SimpleUserCreationForm(UserCreationForm):
    class Meta:
        model = SimpleUser
        fields = ('username', 'email', 'password1', 'password2')
