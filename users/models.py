from django.db import models
from django.contrib.auth.models import User, AbstractUser


# Create your models here.
class SimpleUser(AbstractUser):
    email = models.EmailField(unique=True)
    age = models.PositiveIntegerField(blank=True, null=True)
