from django.test import TestCase
from .models import SimpleUser


class TestSimpleUser(TestCase):

    def setUp(self):
        self.user = SimpleUser.objects.create(username='test', password='test')

    def tearDown(self):
        self.user.delete()

    def test_create(self):
        self.assertEqual(self.user.username, 'test')

    def test_is_not_superuser(self):
        self.assertEqual(self.user.is_superuser, False)
