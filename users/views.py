from django.shortcuts import render
from .models import SimpleUser
from django.views.generic import CreateView
from django.contrib.auth.views import LoginView, LogoutView
from .forms import SimpleUserCreationForm


class UserCreateView(CreateView):
    model = SimpleUser
    template_name = 'users/create.html'
    success_url = '/'
    form_class = SimpleUserCreationForm


class AuthView(LoginView):
    template_name = 'users/login.html'
    success_url = '/'


class SimpleUserLogoutView(LogoutView):
    pass
